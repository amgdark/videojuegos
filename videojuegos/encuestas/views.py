from django.shortcuts import render
from .models import Cuestionario, ContestaCuestionario, DetalleContestaCuestionario
from .forms import AplicaCuestionario


def cuestionario(request):
    quiz = Cuestionario.objects.get(id=1)
    form = AplicaCuestionario(preguntas=quiz.preguntas.all())    
    
    if request.method == 'POST':
        form = AplicaCuestionario(request.POST, preguntas=quiz.preguntas.all()) 
        if form.is_valid():
            data = form.cleaned_data
            
            contesta_cuestionario = ContestaCuestionario.objects.create(
                cuestionario = quiz,
                usuario = request.user
            )
            
            for pregunta in quiz.preguntas.all():
                DetalleContestaCuestionario.objects.create(
                    pregunta = pregunta,
                    respuesta = data[f"pregunta-{pregunta.id}"],
                    contesta_cuestionario = contesta_cuestionario
                )
    context = {
        # 'quiz': quiz,
        'form': form
    }
    return render(request, 'cuestionario.html', context)