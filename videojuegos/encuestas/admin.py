from django.contrib import admin
from . import models


admin.site.register(models.Categoria)
admin.site.register(models.SubCategoria)
admin.site.register(models.Cuestionario)
admin.site.register(models.Pregunta)
admin.site.register(models.ContestaCuestionario)
admin.site.register(models.DetalleContestaCuestionario)
admin.site.register(models.OpcionesPregunta)