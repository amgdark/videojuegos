from django import forms
from .models import ContestaCuestionario


class AplicaCuestionario(forms.Form):
    # class Meta:
    #     model = ContestaCuestionario
    #     fields = '__all__'
        
    def __init__(self, *args, **kwargs):
        preguntas = kwargs.pop('preguntas')
        
        super(AplicaCuestionario, self).__init__(*args, **kwargs)
        
        TIPO_DATO = {
            '1': forms.CharField(max_length=100),
            '2': forms.IntegerField(),
            '3': forms.DecimalField(),
            '4': forms.DateField(widget=forms.DateInput(attrs={'type':'date'})),
            '5': forms.DateTimeField(widget=forms.DateTimeInput(attrs={'type':'datetime'})),
            '6': forms.EmailField(),
            '7': forms.ModelChoiceField(queryset=None),
            '8': forms.ModelChoiceField(queryset=None, widget=forms.RadioSelect()),
        }
        for pregunta in preguntas:
            self.fields[f"pregunta-{str(pregunta.id)}"] = TIPO_DATO[pregunta.tipo_dato]
            if pregunta.tipo_dato in ['7', '8']:
                self.fields[f"pregunta-{str(pregunta.id)}"].queryset = pregunta.opciones_pregunta.all()
            if pregunta.requerida == '0':
                self.fields[f"pregunta-{str(pregunta.id)}"].required = False
            if pregunta.tipo_dato == '1':
                print (pregunta.size_texto)
                self.fields[f"pregunta-{str(pregunta.id)}"].max_length = 150
            self.fields[f"pregunta-{str(pregunta.id)}"].label=pregunta.pregunta  
        # interests = ProfileInterest.objects.filter(
        #     profile=self.instance
        # )
        # for i in range(len(interests) + 1):
        #     field_name = 'interest_%s' % (i,)
        #     self.fields[field_name] = forms.CharField(required=False)
        #     try:
        #         self.initial[field_name] = interests[i].interest
        #     except IndexError:
        #         self.initial[field_name] = “”
        # # create an extra blank field
        # field_name = 'interest_%s' % (i + 1,)
        # self.fields[field_name] = forms.CharField(required=False)
    