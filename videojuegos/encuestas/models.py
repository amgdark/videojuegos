from django.db import models
from django.contrib.auth.models import User


class Categoria(models.Model):
    nombre = models.CharField(max_length=50)
    
    def __str__(self):
        return self.nombre
    
class SubCategoria(models.Model):
    nombre = models.CharField(max_length=50)
    categoria = models.ForeignKey("encuestas.Categoria", verbose_name="Sub-categoría", on_delete=models.CASCADE)
    
    def __str__(self):
        return self.nombre
    

class Cuestionario(models.Model):
    nombre = models.CharField(max_length=50)
    descripcion = models.TextField("Descripción", max_length=50)
    
    subcategoria = models.ForeignKey("encuestas.SubCategoria", verbose_name="Cuestionario", on_delete=models.CASCADE)
    
    def __str__(self):
        return self.nombre
    
SI_NO=[
    ('1','Si'),
    ('0','No'),
]
SIZE_TEXTO = [
    (25,'25'),
    (50,'50'),
    (100,'100'),
    (150,'150'),
    (200,'200'),
    (300,'300'),
    (500,'500'),
    (700,'700'),
    (1000, 'Mucho texto')
]
TIPO_DATO=[
    ('1','Texto'),
    ('2','Númerico'),
    ('3','Decimal'),
    ('4','Fecha'),
    ('5','Fecha y hora'),
    ('6','Correo electrónico'),
    ('7','Selección (Combo)'),
    ('8','Selección (Opciones)'),
]

class Pregunta(models.Model):
    pregunta = models.CharField(max_length=150)
    descripcion = models.TextField("Descripción", max_length=50)
    requerida = models.CharField(choices=SI_NO, max_length=1)
    tipo_dato = models.CharField("Tipo de respuesta", max_length=2, choices=TIPO_DATO)
    size_texto = models.SmallIntegerField("Máximo de palabras apróximadamente", null=True, blank=True, choices=SIZE_TEXTO)
    cuestionario = models.ForeignKey("encuestas.Cuestionario", verbose_name="Pregunta", on_delete=models.CASCADE, related_name='preguntas')
    
    def __str__(self):
        return self.pregunta
    
class OpcionesPregunta(models.Model):
    opcion = models.CharField("Opción", max_length=150)
    valor = models.CharField(max_length=2)
    pregunta = models.ForeignKey("encuestas.Pregunta", verbose_name="Opciones", on_delete=models.CASCADE, related_name='opciones_pregunta')
    
    def __str__(self):
        return self.opcion
    
class ContestaCuestionario(models.Model):
    fecha = models.DateTimeField("Fecha", auto_now=True)
    
    cuestionario = models.ForeignKey("encuestas.Cuestionario", verbose_name="Cuestionario", on_delete=models.DO_NOTHING)
    usuario = models.ForeignKey(User, verbose_name="Usuario", on_delete=models.DO_NOTHING)
    
    def __str__(self):
        return self.cuestionario.nombre + self.usuario.username
    
class DetalleContestaCuestionario(models.Model):
    pregunta = models.ForeignKey("encuestas.Pregunta", verbose_name="Pregunta", on_delete=models.DO_NOTHING)
    respuesta = models.CharField(max_length=1500)
       
    contesta_cuestionario = models.ForeignKey("encuestas.ContestaCuestionario", verbose_name="Contesta", on_delete=models.DO_NOTHING)
     
    
    def __str__(self):
        return self.pregunta.pregunta + self.respuesta
