from dataclasses import fields
from django import forms
from articulos.models import Articulos, Categoria

class FormArticulo(forms.ModelForm):
        
    class Meta:
        model = Articulos
        # fields = ['nombre','genero','stock']
        fields = '__all__'
        # exclude = 'stock'
        
        widgets = {
            'nombre': forms.TextInput(attrs={'class':'form-control'}),
            'decripcion': forms.Textarea(attrs={'class':'form-control', 'rows':5}),
            'stock': forms.NumberInput(attrs={'class':'form-control'}),
            'genero': forms.Select(attrs={'class':'form-control'}),
            'categoria': forms.Select(attrs={'class':'form-control'}),
            
        }
        
class FormCategoria(forms.ModelForm):
        
    class Meta:
        model = Categoria
        # fields = ['nombre','genero','stock']
        fields = '__all__'
        # exclude = 'stock'
        
        widgets = {
            'nombre': forms.TextInput(attrs={'class':'form-control'}),
            'descripcion': forms.Textarea(attrs={'class':'form-control', 'rows':5}),
            
        }
        
class FormFiltroArticulo(forms.Form):
    nombre = forms.CharField(
        widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Nombre'}),
        required=False
    )   
    descripcion = forms.CharField( 
        widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Descripción'}),
        required=False
    )   
    genero = forms.CharField(
        widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Género'}),
        required=False
    )   
    categoria = forms.CharField(
        widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Categoría'}),
        required=False
    )   
    