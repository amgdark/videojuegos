from django.test import TestCase
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User


class TestViesArticulos(TestCase):
    def setUp(self):
        usuario = User(
            username='alex',
            
        )
        usuario.set_password('admin123')
        usuario.save()
        self.client.login(username='alex', password='admin123')
        # self.client.force_login(usuario) 
        
        
    def test_lista_articulos_estatus(self):
        respuesta = self.client.get('/articulos')
        self.assertEquals(respuesta.status_code, 200)
        
        
    def test_formulario_articulos_nuevo(self):
        
        respuesta = self.client.get('/articulos/nuevo')
        self.assertEquals(respuesta.status_code, 200)
        
        
    def test_template_articulo_nuevo(self):
        response = self.client.get('/articulos/nuevo')
        self.assertTemplateUsed(response, 'nuevo_articulo.html')
    