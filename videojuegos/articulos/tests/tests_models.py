from django.test import TestCase
from articulos.models import Articulos, Categoria
from django.core.exceptions import ValidationError


class TestSmoke(TestCase):
    
    def test_smoke(self):
        self.assertEquals(2+2,4)

    def setUp(self):
        self.categoria = Categoria.objects.create(
            nombre='Categoría 1',
            descripcion='Rollo de categoría'
        )
        self.articulo = Articulos.objects.create(
            nombre='Consola',
            decripcion='Rollo',
            stock=5,
            genero='1',
            categoria=self.categoria
        )

    def test_insertar_articulo_cuenta(self):
        self.assertEquals(Articulos.objects.count(),1)

    def test_insertar_articulo_verifica_nombre(self):
        articulo2 = Articulos.objects.get(nombre='Consola')
        self.assertEquals(self.articulo.nombre, articulo2.nombre)

    def test_verifica_longitud_categoria(self):
        self.categoria.nombre = 'k'*110
        
        with self.assertRaises(ValidationError):
            self.categoria.full_clean()
            
    def test_verifica_stock_tenga_numero(self):
        self.articulo.stock = 'k'
        
        with self.assertRaises(ValidationError):
            self.articulo.full_clean()
