# Generated by Django 4.0.2 on 2022-05-26 14:16

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('usuarios', '0002_estado_alter_datospersonales_curp_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='datospersonales',
            name='estado',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.DO_NOTHING, to='usuarios.estado', verbose_name='Estado'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='datospersonales',
            name='municipio',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.DO_NOTHING, to='usuarios.municipio', verbose_name='Municipio'),
            preserve_default=False,
        ),
    ]
