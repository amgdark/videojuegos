from django.contrib import admin
from usuarios.models import Estado, Municipio

admin.site.register(Estado)
admin.site.register(Municipio)