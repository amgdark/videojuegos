Característica: Editar un artículo
    Como administrador del sistema
    quiero editar un artículo
    para contar con un invenario actualizado.

        Escenario: Artículo encontrado y se cambia stock
            Dado que ingreso a la url "http://localhost:8000/articulos/"
              Y la página me redirecciona a login para agregar mi usuario "alex" y contraseña "alex123"
              Y en la sección de artículos busco un artículo nombre "Consola Chida",
              Y presiono el botón de Editar
              Y modifico el stock a "25"
             Cuando presiono el botón "Guardar"
             Entonces puedo ver el artículo "Consola Chida" con el stock en "25".

        # Escenario: Artículo no existente
        #     Dado que ingreso a la url "http://localhost:8000/articulos/"
        #       Y la página me redirecciona a login para agregar mi usuario "alex" y contraseña "alex123"
        #       Y en la sección de artículos busco un artículo nombre "Consola Fea",
             
        
