Característica: Alta de un nuevo artículo
    Como administrador del sistema
    quiero dar de alta un artículo
    para contar con un invenario actualizado.

        Escenario: Datos correctos de artículo
            Dado que ingreso a la url "http://localhost:8000/articulos/nuevo"
              Y la página me redirecciona a login para agregar mi usuario "alex" y contraseña "alex123"
              Y en la sección de artículos agregó los datos nombre "Consola Chida",
              Y descripción "Un rollo de descripción", stock "5", género "Aventura" y categoría "Consolas"
             Cuando presiono el botón "Agregar"
             Entonces puedo ver el artículo "Consola Chida" en la lista de artículos.
