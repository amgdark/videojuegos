from behave import given, then, when
from selenium import webdriver
from selenium.webdriver.common.by import By
import time

@given(u'que ingreso a la url "{url}"')
def step_impl(context, url):
    context.driver = webdriver.Chrome()
    context.driver.get(url)

@given(u'la página me redirecciona a login para agregar mi usuario "{usuario}" y contraseña "{contra}"')
def step_impl(context, usuario, contra):
    context.driver.find_element(By.NAME, 'username').send_keys(usuario)
    context.driver.find_element(By.NAME, 'password').send_keys(contra)
    context.driver.find_element(By.XPATH, '/html/body/div/div/div[2]/form/div[3]/div[2]/button').click()


@given(u'en la sección de artículos agregó los datos nombre "{nombre}",')
def step_impl(context,nombre):
    context.driver.find_element(By.NAME, 'nombre').send_keys(nombre)
    


@given(u'descripción "{descripcion}", stock "{stock}", género "{genero}" y categoría "{categoria}"')
def step_impl(context, descripcion, stock, genero, categoria):
    context.driver.find_element(By.NAME, 'decripcion').send_keys(descripcion)
    context.driver.find_element(By.NAME, 'stock').send_keys(stock)
    context.driver.find_element(By.NAME, 'genero').send_keys(genero)
    context.driver.find_element(By.NAME, 'categoria').send_keys(categoria)


@when(u'presiono el botón "Agregar"')
def step_impl(context):
    context.driver.find_element(By.CLASS_NAME, 'btn-success').click()

@then(u'puedo ver el artículo "{articulo}" en la lista de artículos.')
def step_impl(context, articulo):
    tabla = context.driver.find_element(By.TAG_NAME, 'tbody')
    trs = tabla.find_elements(By.TAG_NAME, 'tr')
    articulos = []
    for tr in trs:
        tds = tr.find_elements(By.TAG_NAME, 'td')
        articulos.append(tds[0].text)
        
    assert articulo in articulos, str(articulos)